package com.sannedak.diceroller.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.ui.tooling.preview.Preview
import com.sannedak.diceroller.R
import com.sannedak.diceroller.ui.DiceRollerTheme

class DiceFragment : Fragment() {

    private val viewModel: DiceViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            DiceRollerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    DiceRoller()
                }
            }
        }
    }

    @Composable
    fun DiceRoller() {
        val diceNumber: Int by viewModel.diceNumber.observeAsState(R.drawable.dice_6)

        Column {
            TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) })
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(asset = vectorResource(id = diceNumber))
                Button(onClick = { viewModel.rollDice() }) {
                    Text(text = stringResource(id = R.string.button_roll))
                }
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        DiceRollerTheme {
            DiceRoller()
        }
    }
}
