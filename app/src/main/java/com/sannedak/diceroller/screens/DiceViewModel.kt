package com.sannedak.diceroller.screens

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sannedak.diceroller.R
import kotlin.random.Random

class DiceViewModel : ViewModel() {

    private var _diceNumber = MutableLiveData(R.drawable.dice_6)

    val diceNumber: LiveData<Int>
        get() = _diceNumber

    fun rollDice() {
        _diceNumber.value = when (Random.nextInt(6)) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
    }
}